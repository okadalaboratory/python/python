# Python

[公式サイト](https://www.python.jp/)

[東大が無料公開している超良質なPython/Data Science/Cloud教材まとめ (*随時更新)](https://digitaldigital.hatenablog.com/entry/2020/07/21/104040)

[Pythonプログラミング入門（東京大学）](https://utokyo-ipp.github.io/)

[「Python ゼロからはじめるプログラミング」サポートページ](https://mitani.cs.tsukuba.ac.jp/book_support/python/)
[Python ゼロからはじめるプログラミング](https://www.shoeisha.co.jp/book/detail/9784798169460)を教科書に使った際に、[改変可能なパワポあり](https://mitani.cs.tsukuba.ac.jp/book_support/python/python_slides.pptx)

[CS50.jp](https://cs50.jp/)
ハーバード大学 CS50 の日本語版翻訳プロジェクト
クリエイティブ・コモンズ 表示-非営利-継承 4.0 インターナショナル (CC BY-NC-SA 4.0) ライセンス

[九州大学 数理・データサイエンス教育研究センター](https://mdsc.kyushu-u.ac.jp/lectures)
CC-BYでパワポあり


## ライセンス
- CC-BY （表示） 
http://creativecommons.org/licenses/by/2.1/jp/ <br>
http://creativecommons.org/licenses/by/2.1/jp/legalcode <br>
原作者のクレジット（氏名、作品タイトルとURL）を表示することを守れば、改変はもちろん、営利目的での二次利用も許可される 

- CC-BY-SA （表示－継承） 
http://creativecommons.org/licenses/by-sa/2.1/jp/<br>
http://creativecommons.org/licenses/by-sa/2.1/jp/legalcode <br>
原作者のクレジット（氏名、作品タイトルとURL）を表示し、改変した場合には元の作品と同じCCライセンス（このライセンス）で公開することを守れば、営利目的での二次利用も許可される 

- CC-BY-NC （表示 －非営利）
http://creativecommons.org/licenses/by-nc/2.1/jp/<br>
 http://creativecommons.org/licenses/by-nc/2.1/jp/legalcode<br>
  原作者のクレジット（氏名、作品タイトルとURL）を表示し、かつ非営利目的であれば、改変したり再配布したりすることができる 

- CC-BY-NC-SA （表示－非営利 －継承） 
http://creativecommons.org/licenses/by-nc-sa/2.1/jp/<br>
 http://creativecommons.org/licenses/by-nc-sa/2.1/jp/legalcode<br>
  原作者のクレジット（氏名、作品タイトルとURL）を表示し、かつ非営利目的に限り、また改変を行った際には元の作品と同じ組み合わせのCCライセンスで公開することを守れば、改変したり再配布したりすることができる 



[QDくん⚡️AI関連の無料教材紹介](https://twitter.com/developer_quant)
https://note.com/quantdeveloper/